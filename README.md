# SatsStack - Infrastructure

Repository for configuration files of k8s resources. This infrastructure is used for hosting SatsStack application. 

## Google Kubernetes Engine

The infrastructure is currently running at [https://cloud.google.com/kubernetes-engine](Google Kubernetes Engine). For deployment is used kubectl command line tool.

## Deployment

1. kubectl apply (-f FILENAME)

## License

[The Unlicense](LICENSE.md)
